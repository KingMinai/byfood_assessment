package answer

func RecursiveHalf(n int) []int {
	// recursivly print half of n until n is 1

	if n == 1 {
		return nil
	}
	return append(RecursiveHalf(n/2), n)

}

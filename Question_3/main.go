package main

import (
	"bufio"
	"example/byfood_q3/answer"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Question 3:")
	fmt.Println("Enter a list of strings with repeated values, separated by spaces:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()

	fmt.Println("Most common:")
	fmt.Println(answer.ArrayMode(strings.Split(input, " ")))
}

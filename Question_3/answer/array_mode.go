package answer

import "strings"

func ArrayMode(input []string) string {
	var mode string
	var maxCount int
	var count int
	for _, v := range input {
		count = strings.Count(strings.Join(input, ""), v)
		if count > maxCount {
			maxCount = count
			mode = v
		}
	}
	return mode
}

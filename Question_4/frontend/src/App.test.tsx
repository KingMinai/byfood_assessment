import { render, screen } from '@testing-library/react';

import App from './App';
import { getAllUsers } from './api/api';

test('Renders all users', async () => {
	const { users } = await getAllUsers();

	render(<App />);

	for (const user of users) {
		const userElement = await screen.findByText(`${user.firstName} ${user.lastName}`);
		expect(userElement).toBeInTheDocument();
	}
});

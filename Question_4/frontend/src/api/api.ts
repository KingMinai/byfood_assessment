import axios from 'axios'

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
})

export const getAllUsers = () => api.get(`/api/users`).then(res => res.data)

export const getUserById = (id: string) => api.get(`/api/user/${id}`).then(res => res.data)

export const createUser = (user: {
  firstName: string
  lastName: string
  role: string
}) => api.post(`/api/user/create`, user).then(res => res.data)

export const updateUser = (id: string, user: {
  firstName?: string
  lastName?: string
  role?: string
}) => api.put(`/api/user/update/${id}`, user).then(res => res.data)

export const deleteUser = (id: string) => api.delete(`/api/user/${id}`).then(res => res.data)
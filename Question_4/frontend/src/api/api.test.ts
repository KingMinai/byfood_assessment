import { getAllUsers, getUserById, createUser, updateUser, deleteUser } from './api';

const testUser = {
  firstName: 'Bruce',
  lastName: 'Wayne',
  role: 'Batman'
}

let createdUserID: string;

test('GET /users', async () => {
  const users = await getAllUsers();

  expect(users.users.length).toBeGreaterThan(0);
});

test('POST /users', async () => {
  const users = await getAllUsers();

  expect(users.users[users.users.length - 1].firstName).not.toBe('Bruce');

  const user = await createUser(testUser);

  expect(user.user.firstName).toBe('Bruce');
  createdUserID = user.user.id;

  const users2 = await getAllUsers();
  expect(users2.users[users2.users.length - 1].firstName).toBe('Bruce');
});

test('GET /users/:id', async () => {
  const user = await getUserById(createdUserID);

  expect(user.user.firstName).toBe('Bruce');
});

test('PUT /users/:id', async () => {
  const user = await updateUser(createdUserID, {
    firstName: 'Bob',
  });

  expect(user.user.firstName).toBe('Bob');
});

test('DELETE /users/:id', async () => {
  const user = await deleteUser(createdUserID);

  expect(user.message).toBe('User deleted successfully');

  const users = await getAllUsers();
  expect(users.users[users.users.length - 1].id).not.toBe(createdUserID);
});
import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';
import { getAllUsers } from './api/api';
import Nav from './components/nav/Nav';
import UserCard from './components/userCard/UserCard';
import UserForm from './components/userForm/UserForm';

interface User {
	id: string;
	firstName: string;
	lastName: string;
	role: string;
}

interface UF {
	type: string;
	id?: string;
	user?: User;
}

const App: React.FC = () => {
	const [users, setUsers] = useState<User[]>([]);
	const [loading, setLoading] = useState(true);
	const [userForm, setUserForm] = useState<UF | null>(null);

	const handleUserForm = (type: string, id?: string, user?: User) => {
		setUserForm({ type, id, user });
	};

	const closeUserForm = () => {
		setUserForm(null);
		getUsers();
	};

	const getUsers = () => {
		setLoading(true);
		getAllUsers().then(res => {
			setUsers(res?.users.reverse());
			setLoading(false);
		});
	};

	useEffect(() => {
		getUsers();
	}, []);

	return (
		<>
			<Nav handlePopup={handleUserForm} />
			<Container>
				<div className='user-info'>
					{users?.map(user => (
						<UserCard key={user.id} userInfo={user} refresh={getUsers} handlePopup={handleUserForm} />
					))}
				</div>
			</Container>
			{userForm && (
				<UserForm type={userForm?.type} handleClose={closeUserForm} id={userForm?.id} user={userForm?.user} />
			)}
		</>
	);
};

export default App;

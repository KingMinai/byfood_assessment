import React from 'react';
import { Container } from 'react-bootstrap';

import { ActionButton } from '../actionButton/ActionButton';

interface Props {
	message: string;
	handleConfirm: () => void;
	handleClose: () => void;
}

export const Confirmation: React.FC<Props> = ({ message, handleConfirm, handleClose }) => {
	return (
		<>
			<div className='popup-box'>
				<Container className='box del'>
					<div>
						<div>{message}</div>
						<div>
							<ActionButton buttonName='Cancel' handleAction={handleClose} />
							<ActionButton buttonName='Confirm' handleAction={handleConfirm} />
						</div>
					</div>
				</Container>
			</div>
		</>
	);
};

export default Confirmation;

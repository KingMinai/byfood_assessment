import React, { useEffect, useRef, useState } from 'react';
import { Container, Form, Alert } from 'react-bootstrap';

import { createUser, updateUser } from '../../api/api';
import { ActionButton } from '../actionButton/ActionButton';

interface Props {
	handleClose: () => void;
	type: string;
	id?: string;
	user?: {
		firstName: string;
		lastName: string;
		role: string;
	};
}

export const UserForm: React.FC<Props> = ({ type, id, user, handleClose }) => {
	const fNameRef = useRef<HTMLInputElement>(null);
	const lNameRef = useRef<HTMLInputElement>(null);
	const roleRef = useRef<HTMLInputElement>(null);

	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		if (user) {
			fNameRef.current!.value = user.firstName;
			lNameRef.current!.value = user.lastName;
			roleRef.current!.value = user.role;
		}
	}, [id, user]);

	const buttonFunction = async () => {
		setError('');
		setLoading(true);
		if (!verifyInput()) {
			setError('Please fill out all fields');
			setLoading(false);
			return;
		}

		if (type === 'new') {
			await createUser({
				firstName: fNameRef.current!.value,
				lastName: lNameRef.current!.value,
				role: roleRef.current!.value,
			});
		} else {
			await updateUser(id!, {
				firstName: fNameRef.current!.value,
				lastName: lNameRef.current!.value,
				role: roleRef.current!.value,
			});
		}
		setError('');
		setLoading(false);
		handleClose();
	};

	function verifyInput() {
		if (fNameRef.current!.value === '') {
			return false;
		}
		if (lNameRef.current!.value === '') {
			return false;
		}
		if (roleRef.current!.value === '') {
			return false;
		}
		return true;
	}

	return (
		<>
			<div className='popup-box'>
				<Container className='box'>
					<h3>{type === 'new' ? 'Create User' : 'Edit user'}</h3>
					<Form>
						<Form.Group>
							<Form.Label>First Name</Form.Label>
							<Form.Control ref={fNameRef} type='text' placeholder='Enter First Name' />
							<Form.Label>Last Name</Form.Label>
							<Form.Control ref={lNameRef} type='text' placeholder='Enter Last Name' />
							<Form.Label>Role</Form.Label>
							<Form.Control ref={roleRef} type='text' placeholder='Enter Role' />
							<div className='btns'>
								<ActionButton disabled={loading} buttonName='< Back' handleAction={handleClose} />
								<ActionButton
									disabled={loading}
									buttonName={type === 'new' ? 'Create' : 'Update'}
									handleAction={buttonFunction}
								/>
							</div>
						</Form.Group>
					</Form>
					{error && <Alert variant='danger'>{error}</Alert>}
				</Container>
			</div>
		</>
	);
};

export default UserForm;

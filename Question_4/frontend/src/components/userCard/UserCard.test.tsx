import React from 'react';
import { render, screen } from '@testing-library/react';

import { UserCard } from './UserCard';
import { act } from 'react-dom/test-utils';

const testUser = {
	id: '1',
	firstName: 'Test',
	lastName: 'User',
	role: 'Test Role',
};

test('User card renders with text', () => {
	console.log = jest.fn();
	render(<UserCard key='0' userInfo={testUser} refresh={console.log} handlePopup={console.log} />);

	const fullName = screen.getByText(/Test User/i);
	const role = screen.getByText(/Test Role/i);

	expect(fullName.textContent).toBe('Test User');
	expect(role.textContent).toBe('Test Role');
});

test('User card edit runs', () => {
	console.log = jest.fn();
	render(<UserCard key='0' userInfo={testUser} refresh={console.log} handlePopup={console.log} />);
	const editButton = screen.getByText(/Edit/i);
	editButton.click();
	expect(console.log).toHaveBeenCalledWith('edit', '1', testUser);
});

test('User card delete runs', async () => {
	console.log = jest.fn();
	render(<UserCard key='0' userInfo={testUser} refresh={console.log} handlePopup={console.log} />);
	const button = screen.getByText(/Delete/i);
	await act(async () => button.click());
	const confirmButton = screen.getByText(/Confirm/i);
	expect(confirmButton.textContent).toBe('Confirm');
});

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { ActionButton } from './ActionButton';

test('Action button calls callback', () => {
  const handleAction = jest.fn();
  render(<ActionButton buttonName="test" handleAction={handleAction} />);

  const button = screen.getByText(/test/i);
  fireEvent.click(button);
  expect(handleAction).toHaveBeenCalled();
});

test('Action button renders with text', () => {
  render(<ActionButton buttonName="test" handleAction={() => { }} />); 

  const button = screen.getByText(/test/i);
  expect(button.textContent).toBe('test');
}); 

export {}

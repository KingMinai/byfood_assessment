import React from 'react';
import { Button } from 'react-bootstrap';

interface Props {
	buttonName: string;
	handleAction: () => void;
	disabled?: boolean;
}

export const ActionButton: React.FC<Props> = ({ buttonName, handleAction, disabled }) => {
	return (
		<>
			<Button disabled={disabled} onClick={handleAction}>
				{buttonName}
			</Button>
		</>
	);
};

import React from 'react';
import { render, screen } from '@testing-library/react';

import { Nav } from './Nav';

test('New user button runs', () => {
	console.log = jest.fn();
	render(<Nav handlePopup={console.log} />);
	const button = screen.getByText(/New \+/i);
	button.click();
	expect(console.log).toHaveBeenCalledWith('new');
});

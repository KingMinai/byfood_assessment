import React from 'react';
import { Navbar, Container } from 'react-bootstrap';

import { ActionButton } from '../actionButton/ActionButton';

interface Props {
	handlePopup: (type: string) => void;
}

export const Nav: React.FC<Props> = ({ handlePopup }) => {
	const handleNew = () => {
		handlePopup('new');
	};

	return (
		<>
			<Navbar expand='lg' variant='light' className='nav'>
				<Container>
					<Navbar.Brand>Userbase</Navbar.Brand>
					<Navbar.Text className='justify-content-end'>
						<ActionButton buttonName='New +' handleAction={handleNew} />
					</Navbar.Text>
				</Container>
			</Navbar>
		</>
	);
};

export default Nav;

package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type User struct {
	ID        uint   `gorm:"primary_key" json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Role      string `json:"role"`
}

var db *gorm.DB

func init() {
	d, err := gorm.Open("sqlite3", "../byfood.db")
	if err != nil {
		panic("failed to connect database")
	}
	db = d

	db.AutoMigrate(&User{})
}

func GetUsers(c *gin.Context) {
	var users []User
	db.Find(&users)

	c.JSON(http.StatusOK, gin.H{
		"users": users,
	})
}

func GetUser(c *gin.Context) {
	userID := c.Param("userID")

	var user User
	db.First(&user, userID)

	c.JSON(http.StatusOK, gin.H{"user": user})
}

func CreateUser(c *gin.Context) {
	var newUser User

	// Validate input
	if err := c.ShouldBindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.Create(&newUser)

	c.JSON(http.StatusCreated, gin.H{
		"message": "User created successfully",
		"user":    newUser,
	})
}

func UpdateUser(c *gin.Context) {
	userID := c.Param("userID")
	var uUser User

	// Validate input
	if err := c.ShouldBindJSON(&uUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user User
	db.First(&user, userID)

	// Update non-empty fields
	if uUser.FirstName != "" {
		user.FirstName = uUser.FirstName
	}
	if uUser.LastName != "" {
		user.LastName = uUser.LastName
	}
	if uUser.Role != "" {
		user.Role = uUser.Role
	}

	db.Save(&user)

	c.JSON(http.StatusOK, gin.H{
		"message": "User updated successfully",
		"user":    uUser,
	})

}

func DeleteUser(c *gin.Context) {
	userID := c.Param("userID")

	var user User
	db.First(&user, userID)
	db.Delete(&user)

	c.JSON(http.StatusOK, gin.H{
		"message": "User deleted successfully",
	})
}

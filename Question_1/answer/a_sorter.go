package answer

import (
	"reflect"
	"strings"
)

func A_Sorter(arr []string) []string {
	aArray := []string{}
	nArray := []string{}

	// split array
	for _, v := range arr {
		if strings.Contains(v, "a") {
			// array contains a's
			aArray = append(aArray, v)
		} else {
			// array does not contain a's
			nArray = append(nArray, v)
		}
	}

	// sort arrays
	sortA(aArray)
	sortNormal(nArray)

	// join and return array
	return append(aArray, nArray...)
}

func sortA(arr []string) {
	swap := reflect.Swapper(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr); j++ {
			if strings.Count(arr[i], "a") > strings.Count(arr[j], "a") {
				swap(i, j)
			}
		}
	}
}

func sortNormal(arr []string) {
	swap := reflect.Swapper(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr); j++ {
			// get the length of the strings
			iLenght := len(arr[i])
			jLenght := len(arr[j])

			if iLenght > jLenght {
				// swap if the length is greater
				swap(i, j)
			} else if iLenght == jLenght {
				// swap if string is "alphabetically" greater
				if arr[i] < arr[j] {
					swap(i, j)
				}
			}
		}
	}
}
